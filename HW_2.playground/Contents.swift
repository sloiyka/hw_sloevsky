import UIKit

/* 1) reate a variable named counter and set it equal to 0.
 Create a while loop with the condition counter < 10 which prints out counter is X (where X is replaced with counter value) and then increments counter by 1. */

var counter = 0

while counter < 10 {
    counter += 1
    
}
print(counter)

/* Task 2
 Create a variable named counter and set it equal to 0. Create another variable named roll and set it equal to 0.
 Create a repeat-while loop. Inside the loop, set roll equal to Int.random(in: 0...5) which means to pick a random number between 0 and 5.
 Then increment counter by 1. Finally, print After X rolls, roll is Y where X is the value of counter and Y is the value of roll.
 Set the loop condition such that the loop finishes when the first 0 is rolled. */

var counter1 = 0
var roll = 0

repeat {
    roll = Int.random(in: 0...5)
    counter1 += 1
    print("After", counter1, "rolls, roll is", roll)
}
while roll != 0

/* Task 3
 Create a constant named range, and set it equal to a range starting at 1 and ending with 10 inclusive.
 Write a for loop that iterates over this range and prints the square of each number. */

let range = 1...10

for i in range {
    print("\(i * i)")
}

/*Task 4
 Below you can see an example of for loop that iterates over only the even rows like so:
 
 var sum = 0
 for row in 0..<8 {
 if row % 2 == 0 {
 continue
 }
 
 for column in 0..<8 {
 sum += row * column
 }
 }
 print(sum)
 
 Change this to use a where clause on the first for loop to skip even rows instead of using continue. Check that the sum is 448 as in the initial example. */

var sum = 0
for row in 0..<8 where row % 2 != 0 {
    for column in 0..<8 {
        sum += row * column
    }
}
print(sum)

/*Task 5
 
 Print a table of the first 10 powers of 2 */

let number = 2
var powers = 1

for i in 0...10 {
    if i > 0 {
        for _ in 1...i {
            powers *= number
        }
        print("\(number) powers \(i) = \(powers)")
        powers = 1
    }
    else {
        print("\(number) powers \(i) = 1")
    }
}

/*
 Task 6
 Given a number n, calculate the factorial of n.
 Example: 4 factorial is equal to 1 * 2 * 3 * 4. */

var n = 9
var factorial = 1
for i in 1...n {
    factorial *= i
}
print("Factorial of", n," = ", factorial)

/* Task 7
 
 Given a number n, calculate the n-th Fibonacci number. (Recall Fibonacci is 1, 1, 2, 3, 5, 8, 13, ... Start with 1 and 1 and add these values together to get the next value. The next value is the sum of the previous two. So the next value in this case is 8+13 = 21.) */

var num = 12

var previousN: Int = 0
var currentN: Int = 1
var tempN: Int = 0
var fibnumb: String = ""

print("Fibonacci number of", num, "is: ")

fibnumb += String(previousN)
fibnumb += ","
fibnumb += String(currentN)
fibnumb += ","

for _ in 1...n-1 {
    fibnumb += String(previousN + currentN)
    fibnumb += ","
    tempN = currentN
    currentN = previousN + currentN
    previousN = tempN
}
print(fibnumb)


/*
 Task 8
 Write a switch statement that takes an age as an integer and prints out the life stage related to that age. You can make up the life stages, or use my categorization as follows: 0-2 years, Infant; 3-12 years, Child; 13-19 years, Teenager; 20-39, Adult; 40-60, Middle aged; 61+, Elderly. */

var lifeStage = 67
switch lifeStage {
case 0...2:
    print("Age", lifeStage, "is an Infant")
case 3...12:
    print("Age", lifeStage, "is a Child")
case 13...19:
    print("Age", lifeStage, "is a Teen")
case 20...39:
    print("Age", lifeStage, "is an Adult")
case 40...60:
    print("Age", lifeStage, "is a Middle aged")
    
default:
    print("Age", lifeStage, "is an elderly")
}

/*
 Task 9
 Write a switch statement that takes a tuple containing a string and an integer. The string is a name, and the integer is an age. Use the same cases that you used in the previous exercise and let syntax to print out the name followed by the life stage. For example, for myself it would print out "Slava is an adult." */

var myLifeStage = ("Kiril", 24)

print("The age of", myLifeStage.0, myLifeStage.1)

switch myLifeStage.1 {
case 0...2:
    print(myLifeStage.0, "is an infant")
case 3...12:
    print(myLifeStage.0, "is an child")
case 13...19:
    print(myLifeStage.0, "is an teen")
case 20...39:
    print(myLifeStage.0, "is an adult")
case 40...60:
    print(myLifeStage.0, "is an middle man")
default:
    print(myLifeStage.0, "is an elderly")
}

/*
 Task 10
 Create a constant called myAge and set it to your age. Then, create a constant named isTeenager that uses Boolean logic to determine if the age denotes someone in the age range of 13 to 19. */

let myAge = 24
let isTeenager = myAge >= 13 && myAge <= 19

print("I'm teenager", isTeenager)

/*
 Task 11
 Create another constant named theirAge and set it to my age, which is 30. Then, create a constant named bothTeenagers that uses Boolean logic to determine if both you and I are teenagers.
 */

let theirAge = 30
let bothTeenagers = isTeenager && (theirAge >= 13 && theirAge <= 19)
print("My age is", myAge, "Their age is", theirAge, "\n Some of us are teenager -", bothTeenagers)

/*Task 12
 Create a constant named reader and set it to your name as a string. Create a constant named author and set it to my name, Matt Galloway. Create a constant named authorIsReader that uses string equality to determine if reader and author are equal.
 */

let reader = "Kiril"
let author = "Matt Galloway"
let isAuthorReader = reader == author
print("Reader is", reader)
print("Author is", author)
if isAuthorReader {
    print("Reader and author are equal")
}
else {
    print("Reader and author are not equal")
}

/*
 Task 13
 Create the "days in months array":
 Print the items that contain the number of days in the corresponding month using the for loop and this array.
 */

var dayInMobth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

for i in 1...12 {
    print("In", i, "Months number of days =", dayInMobth[i-1])
}

/*
Task 14
 Create in if and switch separately in a program that will look at the age of a person and say where to go to school, to kindergarten, to school, to work or to retire, and so on.
 */

let age = 69
print("Age =", age)

if age > 0 && age <= 6 {
    print ("This man go to kindergarten")
}
else if age >= 7 && age <= 17 {
    print ("This man go to school")
}
else if age >= 18 && age <= 23 {
    print ("This man go to university ")
}
else if age >= 24 && age <= 59 {
    print ("This man go to work ")
}
else {
    print ("This man go to pension")
}

switch age {
case 0...6:
    print ("This man go to kindergarten")
case 7...17:
    print ("This man go to school")
case 18...23:
    print ("This man go to university ")
case 24...59:
    print ("This man go to work ")
    
default:
    print ("This man go to pension")
}
