import UIKit


/*
 1. Write a function named printFullName that takes two strings called firstName and lastName.
 The function should print out the full name defined as firstName + " " + lastName.
 Use it to print out your own full name.
 */

func printFullName(firstName: String, lastName: String) {
    print("\(firstName) \(lastName)")
}
printFullName(firstName: "Kiril", lastName: "Sloevsky")

/*
 2. Создайте функцию, которая принимает параметры и вычисляет площадь круга.
 */

func areaOfCircle(squaredRadius: Double, piNumber: Double) {
    let circleArea = (squaredRadius * squaredRadius) * piNumber
    print("(\(squaredRadius) * \(squaredRadius)) * \(piNumber) = \(circleArea)")
}
areaOfCircle(squaredRadius: 6, piNumber: 3.14)

/*
 3. Создайте функцию, которая принимает параметры и вычисляет расстояние между двумя точками.
*/

func distanceBetweenPoints(_ x1: Double, _ x2: Double,_ y1: Double,_ y2: Double) {
    let distance = sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2))
    print(distance)
}
distanceBetweenPoints(5.2, 1.2, 5.3, 1.3)

/* 4. Напишите функцию, которая считает факториал числа. */

func factorial(_ n: Int) -> Int {
    if n == 0 {
        return 1
    }
    else {
        return n * factorial(n - 1)
    }
}
print(factorial(7))

/* 5. Напишите функцию, которая вычисляет N-ое число Фибоначчи */

func fibonacchi(_ n: Int) -> Int {
    var prevN = 0
    var currN = 1
    var tempN = 0
    
    for _ in 1...n - 2 {
        tempN = currN
        currN = currN + prevN
        prevN = tempN
    }
    return currN
}

print(fibonacchi(9))

/* 6. First, write the following function:
 func isNumberDivisible(_ number: Int, by divisor: Int) -> Bool
 You’ll use this to determine if one number is divisible by another. It should return true when number is divisible by divisor.
 Hint: You can use the modulo (%) operator to help you out here.
 Next, write the main function:
 func isPrime(_ number: Int) -> Bool
 This should return true if number is prime, and false otherwise. A number is prime if it’s only divisible by 1 and itself.
 You should loop through the numbers from 1 to the number and find the number’s divisors.
 If it has any divisors other than 1 and itself, then the number isn’t prime. You’ll need to use the isNumberDivisible(_:by:) function you wrote earlier.
 Use this function to check the following cases:
 isPrime(6) // false
 isPrime(13) // true
 isPrime(8893) // true
 Hint 1: Numbers less than 0 should not be considered prime. Check for this case at the start of the function and return early if the number is less than 0.
 Hint 2: Use a for loop to find divisors. If you start at 2 and end before the number itself, then as soon as you find a divisor, you can return false.
 */

func isNumberDivisible (_ number: Int, by divisor: Int) -> Bool {
    if number % divisor == 0 {
        return true
    }
    return false
}

func isPrime (_ number: Int) -> Bool {
    if number <= 0 {
        return false
    }
    else if number <= 3 {
        return true
    }
    var i = 2
    while i * i <= number {
        if number % i == 0 {
            return false
        }
        i += 1
    }
    return true
}
print(isPrime(8887))
